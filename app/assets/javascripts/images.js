$(function() {
  Dropzone.options.dropzone = {
    paramName: 'image[data]',
    maxFilesize: 50,
    accept: function(file, done) {
      if (file.name === 'justinbieber.jpg') {
        done('Naha, you don\'t.');
      } else {
        done();
      }
    }
  };
});
