class Image < ActiveRecord::Base
  belongs_to :gallery
  has_many :comments

  MiniExiftool.pstore_dir = Rails.root.join('tmp').to_s

  has_attached_file :data,
    :path => ":rails_root/uploads/:attachment/:gallery_id/:id/:basename_:style.:extension",
    #:url => '/uploads/:attachment/:user_id/:basename_:style.:extension',
    :styles => {
      :preview  => ['200>',  :jpg, :quality => 70],
      :medium => ['1600>',  :jpg, :quality => 70],
      :large => ['1920>', :jpg, :quality => 70]
    }


  validates_attachment_content_type :data, :content_type => /\Aimage\/.*\Z/


  def convert_gps(gps_data)
    data = gps_data.match(/(\d+) deg (\d+)\' (\d+.\d+)/)
    data[1].to_f + (data[2].to_f / 60) + (data[3].to_f / 3600)
  end
end
