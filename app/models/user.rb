class User < ActiveRecord::Base
  has_many :galleries
  has_many :comments

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         :omniauthable,
         omniauth_providers: [:facebook]

  has_attached_file :avatar,
                    path: ':rails_root/public/system/:attachment/:id/:basename_:style.:extension',
                    url: '/system/:attachment/:id/:basename_:style.:extension',
                    styles: {
                      thumb: ['50x50#', :jpg, quality: 70],
                      preview: ['480x480#', :jpg, quality: 70],
                      large: ['600>', :jpg, quality: 70],
                      retina: ['1200>', :jpg, quality: 30]
                    },
                    convert_options: {
                      thumb: '-set colorspace sRGB -strip',
                      preview: '-set colorspace sRGB -strip',
                      large: '-set colorspace sRGB -strip',
                      retina: '-set colorspace sRGB -strip -sharpen 0x0.5'
                    }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
      user.name = auth.info.name   # assuming the user model has a name
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end
end
