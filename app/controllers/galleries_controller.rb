class GalleriesController < ApplicationController
  before_action :set_gallery, only: [:show, :edit, :update, :destroy, :download]

  # GET /galleries
  # GET /galleries.json
  def index
    @galleries = Gallery.all
  end

  # GET /galleries/1
  # GET /galleries/1.json
  def show
  end

  # GET /galleries/new
  def new
    if user_signed_in?
      @gallery = Gallery.new
    else
      redirect_to new_user_session_path
    end
  end

  # GET /galleries/1/edit
  def edit
    @gallery.user.email
  end

  # POST /galleries
  # POST /galleries.json
  def create
    @gallery = Gallery.new(gallery_params)

    respond_to do |format|
      if @gallery.save
        format.html { redirect_to galleries_path, notice: 'Gallery was successfully created.' }
        format.json { render :show, status: :created, location: @gallery }
      else
        format.html { render :new }
        format.json { render json: @gallery.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /galleries/1
  # PATCH/PUT /galleries/1.json
  def update
    respond_to do |format|
      if @gallery.update(gallery_params)
        format.html { redirect_to galleries_path, notice: 'Gallery was successfully updated.' }
        format.json { render :show, status: :ok, location: @gallery }
      else
        format.html { render :edit }
        format.json { render json: @gallery.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /galleries/1
  # DELETE /galleries/1.json
  def destroy
    @gallery.destroy
    respond_to do |format|
      format.html { redirect_to galleries_path, notice: 'Gallery was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def download
    input_path = @gallery.images.first.data.path.match(/(.*)\/\d+\//)[1]
    tmp_dir = "#{Rails.root}/tmp/download/#{@gallery.id}"
    tmp_file = "#{tmp_dir}/download.zip"
    FileUtils.mkdir_p(tmp_dir) unless File.directory?(tmp_dir)
    File.delete(tmp_file) if File.exist?(tmp_file)
    GalleriesHelper::ZipFileGenerator.new(input_path, tmp_file).write
    send_file tmp_file, :type=>"application/zip", :x_sendfile=>true
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gallery
      begin
        @gallery = current_user.galleries.find params[:id]
      rescue
        flash[:error] = "Access denied."
        redirect_to root_url
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gallery_params
      params[:gallery][:user_id] = current_user.id
      params.require(:gallery).permit(:name, :is_public, :user_id)
    end
end
