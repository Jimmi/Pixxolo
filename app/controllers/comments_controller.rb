class CommentsController < ApplicationController

  def new
    if user_signed_in?
      @image = Image.find params[:image_id]
      @comment =  @image.comments.build
    else
      redirect_to new_user_session_path
    end
  end

  # POST /comments
  # POST /comments.json
  def create
    image = Image.find params[:image_id]
    @comment = Comment.new(comment_params)
    respond_to do |format|
      if @comment.save
        format.html { redirect_to galleries_path, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  def comment_params
    params[:comment][:user_id] = current_user.id
    params[:comment][:image_id] = params[:image_id]
    params.require(:comment).permit(:title, :body, :user_id, :image_id)
  end

end
