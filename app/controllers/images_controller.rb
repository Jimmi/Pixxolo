class ImagesController < ApplicationController
  before_action :set_image, only: [:edit, :update, :destroy]
  helper_method :convert_gps
  before_filter :find_gallery

  def get_image
    @image = @gallery.images.find(params[:id])
    send_file @image.data.path(params[:size]), disposition: 'inline', type: 'image/jpeg', x_sendfile: true
  end

  def convert_gps(gps_data)
    data = gps_data.match(/(\d+) deg (\d+)\' (\d+.\d+)/)
    data[1].to_f + (data[2].to_f / 60) + (data[3].to_f / 3600)
  end

  def change_public
    image = @gallery.images.find params[:id]
    if image[:is_public]
      image[:is_public] = false
    else
      image[:is_public] = true
    end

    respond_to do |format|
      if image.save
        if image[:is_public]
          format.html { redirect_to :back, notice: "You changed this image to PUBLIC"}
          format.json { render json: @image }
        else
          format.html { redirect_to :back, notice: "You changed this image to PRIVATE" }
          format.json { render json: @image }
        end
      else
        format.html { redirect_to :back, notice: 'Something went wrong! Please try it again.' }
        format.json { render json: @image }
      end
    end
  end


  # GET /images
  # GET /images.json
  def index
    @images = @gallery.images
  end

  # GET /images/1
4  # GET /images/1.json
  def show
    @image = @gallery.images.find(params[:id])
  end

  # GET /images/new
  def new
    if user_signed_in?
      if current_user.id == @gallery.user_id
        @image = @gallery.images.build
      else
        flash[:error] = "Access denied."
        redirect_to :back
      end
    else
      redirect_to new_user_session_path
    end
  end

  # GET /images/1/edit
  def edit
    @image = @gallery.images.find(params[:id])
  end

  # POST /images
  # POST /images.json
  def create
    @image = @gallery.images.build(image_params)
    if @image.save
      render json: { message: "success"}, :status => 200
    else
      render json: { error: @post.errors.full_messages.join(',')}, :status => 400
    end
  end

  # PATCH/PUT /images/1
  # PATCH/PUT /images/1.json
  def update
    @image = @gallery.images.find(params[:id])
    respond_to do |format|
      if @image.update(image_params)
        format.html { redirect_to gallery_images_path, notice: 'Image was successfully updated.' }
        format.json { render :show, status: :ok, location: @image }
      else
        format.html { render :edit }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /images/1
  # DELETE /images/1.json
  def destroy
    @image = @gallery.images.find(params[:id])
    @image.destroy
    respond_to do |format|
      format.html { redirect_to gallery_images_path, notice: 'Image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def find_gallery
      begin
        @gallery = Gallery.find(params[:gallery_id]) if Gallery.find(params[:gallery_id]).is_public
        if @gallery.nil?
          @gallery = current_user.galleries.find params[:gallery_id]
        end
      rescue
        flash[:error] = "Access denied."
        redirect_to root_url
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_image
      begin
        @gallery = current_user.galleries.find params[:gallery_id]
        @image = @gallery.images.find(params[:id])
      rescue
        flash[:error] = "Access denied."
        redirect_to root_url
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def image_params
      params.require(:image).permit(:gallery_id, :data, :is_public)
    end
end
