json.array!(@galleries) do |gallery|
  json.extract! gallery, :id, :name, :is_public, :user_id
  json.url gallery_url(gallery, format: :json)
end
