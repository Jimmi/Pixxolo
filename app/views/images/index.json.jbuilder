json.array!(@images) do |image|
  json.extract! image, :id, :gallery_id, :data, :is_public
  json.url image_url(image, format: :json)
end
