module PermissionHelper
  def is_gallery_owner? gallery
    current_user.id == gallery.user_id
  end

  def is_image_owner? gallery, image
    (image.gallery_id == gallery.id) && (is_gallery_owner? gallery)
  end
end
