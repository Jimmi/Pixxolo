class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer :gallery_id, null: false
      t.attachment :data
      t.boolean :is_public, default: false, null: false

      t.timestamps null: false
    end
  end
end
