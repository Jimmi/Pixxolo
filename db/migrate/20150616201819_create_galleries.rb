class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :name, null: false
      t.boolean :is_public, default: false, null: false
      t.integer :user_id, null: false

      t.timestamps null: false
    end
  end
end
