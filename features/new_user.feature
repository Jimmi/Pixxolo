Feature: Test user managment
  In order to handle user
  As an author
  I want to test users

  Scenario: register user
    Given I visit the mainpage
    When I press register button
    Then I insert email "test@rl-hosting.de"
    Then I insert name "TestNAme"
    Then I insert password "test1234"
    Then I insert password confirmation "test1234"
    And I save
