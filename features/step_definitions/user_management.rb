Given(/^I visit the mainpage$/) do
  visit '/'
end

When(/^I press register button$/) do
  click_link 'registration'
end

Then(/^I insert email "(.*?)"$/) do |email|
  fill_in 'user_email', with: email
end

Then(/^I insert name "(.*?)"$/) do |name|
  fill_in 'user_name', with: name
end

Then(/^I insert password "(.*?)"$/) do |password|
  fill_in 'user_password', with: password
end

Then(/^I insert password confirmation "(.*?)"$/) do |password|
  fill_in 'user_password_confirmation', with: password
end

Then(/^I save$/) do
  click_button 'submit'
end
