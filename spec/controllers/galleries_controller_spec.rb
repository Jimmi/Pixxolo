require 'rails_helper'

RSpec.describe GalleriesController, type: :controller do
  describe 'GET #index' do
    it 'renders the index' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe 'GET #show' do
    context 'user logged in' do
      it 'show user his gallery' do
        user = login_user
        gallery = create(:gallery, user: user)
        get :show, id: gallery
        expect(assigns(:gallery)).to eq(gallery)
      end

      it 'show not other galleries' do
        login_user
        gallery = create(:gallery)
        get :show, id: gallery
        expect(assigns(:gallery)).not_to eq(gallery)
      end
    end    
  end

end
