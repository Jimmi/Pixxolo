FactoryGirl.define do
  sequence :email do |n|
    "person#{n}@example.com"
  end

  factory :user do
    email
    password  "1234qwer"
    name "testuser1"
  end
end
