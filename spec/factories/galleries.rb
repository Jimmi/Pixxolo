FactoryGirl.define do
  factory :gallery do
    user
    is_public false
    name "Testgallery"
  end
end
