FactoryGirl.define do
  factory :image do
    gallery
    is_public false
  end
end
