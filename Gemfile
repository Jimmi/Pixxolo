source 'https://rubygems.org'

gem 'devise'
gem 'devise-bootstrap-views'
gem 'omniauth-facebook'

gem 'haml'
gem 'haml-rails'

gem 'rubyzip'

gem 'paperclip'
gem 'aws-sdk'
gem 'photoswipe-rails'

gem 'figaro'

gem 'pg'

gem 'mini_exiftool'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails'

# Use SCSS for stylesheets
gem 'sass-rails'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier'
# Use CoffeeScript for .coffee assets and views
# gem 'coffee-rails'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-turbolinks'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'

# Use ActiveModel has_secure_password
# gem 'bcrypt'
gem 'puma'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Testing
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'capybara'

  gem 'database_cleaner'

  gem 'guard'
  gem 'guard-livereload'
  gem 'guard-bundler'
  gem 'guard-rspec'
  gem 'guard-rails'
  gem 'guard-migrate'

  gem 'pry'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  gem 'mina'
  gem 'mina-puma', :require => false
  gem 'mina-nginx', :require => false
  gem 'mina-scp', require: false
end
