# Paperclip::Attachment.default_options[:url] = ':s3_domain_url'
# Paperclip::Attachment.default_options[:path] = '/:class/:attachment/:id_partition/:style/:filename'
module Paperclip
  module Interpolations
    def gallery_id(attachment, style)
      attachment.instance.gallery_id
    end
  end
end
