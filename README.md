# Pixxolo

## Setup

### Requirements

* Ruby on Rails >= 4
* imagemagick
* exiftool

    sudo apt-get install imagemagick postgresql-server-dev-all nodejs libimage-exiftool-perl

### Installation

    git clone git@gitlab.com:Jimmi/Pixxolo.git
    bundle install
    bundle install rake db:migrate RAILS_ENV=development

## TODO

* Add comment function
* Add Captcha
* Add Photoswipe
* Add Image Folders
* Share Folder with password
* Download Fotos
